#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define TAMANO_MAX 100
#define MAX 5
#define MAX2 10

void reemplazar(char linea[], char viejo, char nuevo){
   int i;
   for(i=0;i<strlen(linea);i++){
      if(linea[i]==viejo){
        linea[i]=nuevo;
      }
   }
}

int main()
{

   char cant[MAX];
   char n[MAX2];
   int acumulador=0;
   float prom=0;
   float num;

	float total;


   while(num!=-1){
      printf("\n?");
      fgets(n,MAX2,stdin);
      reemplazar(n,'\n','\0');
      num = atof(n);
	if(num==-1){

          break;

      	}

      total +=num;
      acumulador++;
   }


   prom=total/acumulador;
   printf("\nEl promedio es ");
   printf("%.3f",prom);
   printf("\n");
   return 0;
}
